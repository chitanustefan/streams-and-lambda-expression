package tema5.tema5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;

import javax.xml.crypto.Data;

public class MonitoredData {
	
	LocalDateTime startTime;
	LocalDateTime finishTime;
	String act;
	
	public MonitoredData(LocalDateTime date, LocalDateTime finishTime, String act) {
		this.startTime = date;
		this.finishTime = finishTime;
		this.act = act;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(LocalDateTime finishTime) {
		this.finishTime = finishTime;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}
	
	public Duration getDuration() {
		 Duration duration = Duration.between(this.getStartTime(), this.finishTime);	
		 return duration;
	}
	
}
