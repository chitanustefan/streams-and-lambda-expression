package tema5.tema5;

import java.awt.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;


public class App 
{
	private static Map<String, Long> map1 = new HashMap<String, Long>();
	public static ArrayList<MonitoredData> list = new ArrayList<MonitoredData>();
	
    public static void main( String[] args ) throws IOException
    {
    	String file = "Activities.txt";
    	
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    	DateTimeFormatter formatDays = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	
    	Map<String, Map<String, Long>> map2 = new HashMap<String, Map<String, Long>>();
    	Map<String, Duration> map3 = new HashMap<String, Duration>();
    	Map<String, Long> mapFilter = new HashMap<String, Long>();
    	Map<String, Long> map4 = new HashMap<String, Long>();
    	ArrayList<String> list90 = new ArrayList<String>();
    	
        try (Stream<String> stream = Files.lines(Paths.get(file))) {

        	list = (ArrayList<MonitoredData>) stream.map(s -> s.split("\\t\\t"))
        			.map(arr -> new MonitoredData(LocalDateTime.parse(arr[0], formatter),LocalDateTime.parse(arr[1], formatter),arr[2]))
        			.collect(Collectors.toCollection(ArrayList::new));
           
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Number of activities:" + list.size());
        long distinctDays = list.stream().map(a -> a.getStartTime().format(formatDays)).distinct().count();
        System.out.println("Number of distinct days:"+ distinctDays);
        map1 = list.stream().collect(groupingBy(a-> a.getAct(), counting()));
     //   printHash(map1);
        System.out.println("\n");
        map2 = list.stream().collect(groupingBy(a -> a.getStartTime().format(formatDays), groupingBy(MonitoredData::getAct, counting() ) ) );
     //   printMap(map2);
        map3 = list.stream()
        	    .collect(toMap(a-> a.getAct(), a-> a.getDuration(), Duration::plus));
        
        mapFilter = map3.entrySet().stream().filter(a -> a.getValue().toHours() >= 10)
        		.collect(toMap(a-> a.getKey(), a-> a.getValue().toHours()));
     //   printHash(mapFilter);
        map4 = list.stream().filter(a-> a.getDuration().toMinutes() <= 5).collect(groupingBy(a-> a.getAct(), counting()));
        printHash(map4);
        list90 =  map4.entrySet().stream().filter(a -> a.getValue() >= 0.9 * map1.get(a.getKey() ) ).map(x->x.getKey()).collect(Collectors.toCollection(ArrayList::new));
        
        for(int i=0; i<list90.size();i++) {
        	System.out.println("\n"+list90.get(i));
        }
        
        File f = new File ("results.txt");
        if (!f.exists()) {
            f.createNewFile();
        }
        FileWriter fw = new FileWriter(f.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("Number of activities:" + list.size() + System.getProperty("line.separator")); 
        bw.write("Number of distinct days:"+ distinctDays + System.getProperty("line.separator"));
        bw.write(System.getProperty("line.separator"));
        bw.write("How many times appear in the log" + System.getProperty("line.separator") );
	    for (HashMap.Entry<String, Long> entry : map1.entrySet()) {
		       bw.write(entry.getKey() + ": " + entry.getValue() + System.getProperty("line.separator"));  
		}
	    bw.write(System.getProperty("line.separator"));
	    bw.write("How many times appear each day"  );
	    for (Entry<String, Map<String, Long>> entry : map2.entrySet()) {
	           bw.write(System.getProperty("line.separator"));
		       bw.write(entry.getKey());
		       bw.write(System.getProperty("line.separator"));
			    for (HashMap.Entry<String, Long> entry2 : entry.getValue().entrySet()) {
				       bw.write(entry2.getKey() + ": " + entry2.getValue() + System.getProperty("line.separator"));  
				}
			    bw.write(System.getProperty("line.separator"));
		    }
	   
	    bw.write("Activities with duration larger than 10 hours"  );
	    bw.write(System.getProperty("line.separator"));
	    for (HashMap.Entry<String, Long> entry : mapFilter.entrySet()) {
		       bw.write(entry.getKey() + ": " + entry.getValue() + System.getProperty("line.separator"));  
		}
        bw.close();
      
    }
    
	public static void printDur(Map<String, Duration> map1) {
	    for (HashMap.Entry<String, Duration> entry : map1.entrySet()) {
	       System.out.println(entry.getKey() + ": " + entry.getValue().toHours());
	       
	    }
	}
    
	public static void printHash(Map<String, Long> map1) {
	    for (HashMap.Entry<String, Long> entry : map1.entrySet()) {
	       System.out.println(entry.getKey() + ": " + entry.getValue());
	       
	    }
	}
	
	public static void printMap(Map<String, Map<String, Long>> map2) {
	    for (Entry<String, Map<String, Long>> entry : map2.entrySet()) {
	       System.out.println(entry.getKey());
	       printHash(entry.getValue());
	       System.out.println("\n");
	    }
	}  	
}
